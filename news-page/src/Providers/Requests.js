import axios from 'axios';
import { APIKey, baseURL } from '../Common/Common';



const instance = axios.create({
    baseURL: `${baseURL}`,
    timeout: 30000,
    headers: { 
        Authorization : `Bearer ${APIKey}` },
});

instance.interceptors.response.use(response => response, (error) => {
    if (axios.isCancel(error)) {
        console.log('Request canceled', error.mesage);
        return Promise.reject(null);
    }

    return Promise.reject(error);
});


const getTopStories = (page) => {
    const url = page ? `top-headlines?pageSize=20&language=en&category=${page}` :  'top-headlines?language=en'
    return instance.get(url);
}

export default  getTopStories;