import React, { useEffect, useState } from 'react';
import Navigation from '../src/Components/Navigation/Navigation';
import  getTopStories  from '../src/Providers/Requests';
import Articles from '../src/Components/Articles/Article';
import Loader from '../src/Components/Loader/Loader';
import {shuffle} from 'lodash';
import "./App.css"

const App = () => {
    const [topStories, setTopStories] = useState([]);
    const [page,setPage] = useState(null);
    const [appState, setAppState] = useState({
        loading: true,
        error: false
    });


    useEffect(() => {
        setAppState(a => ({ ...a, loading: true }));
        getTopStories(page)
        .then(response => {
            if (response && response.data && response.data.status === 'ok') {
                setTopStories(shuffle(response.data.articles));
            }
        }, error => {
            console.log(error);
            setAppState(a => ({ ...a, error: true }));
        })
        .finally(_ => {
            setAppState(a => ({ ...a, loading: false }));
        });
    }, [page]);


    const Content = appState.loading ? <Loader /> : <Articles topStories={topStories} />;
    return (

        <div className="content-wrapper">
            <Navigation onChangePage={(page)=>setPage(page)} />
            {Content}
        </div>

    )
}

export default App;