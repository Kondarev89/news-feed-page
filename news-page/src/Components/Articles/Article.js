import React from 'react';
import Slider from 'react-slick';
import Background from "./Background"

const Articles = ({ topStories }) => {

    const news = topStories.map((article, index) => <Background article={article} key={index} />)

    return <Slider
        lazyLoad={true}
        arrows={false} children={news}
        dots={true}
        speed={800}
        pauseOnDotsHover={false}
        pauseOnHover={false}
        pauseOnFocus={false}
        swipeToSlide={true}
        autoplaySpeed={10000}
        autoplay={true} />

}

export default Articles;