import React, { useEffect } from 'react';
    import './Nav.css';
import { menuItems } from "../../Common/Common";

const Navigation = ({ onChangePage }) => {
    const [activeMenu, setActiveMenu] = React.useState(menuItems[0]);
    useEffect(() => {
        document.title = `News-Feed | ${activeMenu}`
    }, [activeMenu]);

    const menuClick = (menuItem) => {
        setActiveMenu(menuItem);
        onChangePage(menuItem === 'top news' ? null : menuItem);
    }


    const menu = menuItems.map(menuItem =>
        <li className={activeMenu === menuItem ? 'active' : null} key={menuItem} id="one">
            <button onClick={() => menuClick(menuItem)}>{menuItem} </button>
        </li>
    );
    return (
        <div className="m-container" >
            <ul children={menu} />
        </div>
    )
}


export default Navigation;